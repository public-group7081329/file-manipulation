﻿Param([parameter(Mandatory=$true,
   HelpMessage="Source Directory & Drive")]
   $dir
   )
$d = Get-ChildItem $dir\*.jpg -recurse | Where-Object {$_.psIsContainer -eq $false}| resolve-path  |  get-random -count 1
Copy-Item $d  -destination $dir\folder.jpg