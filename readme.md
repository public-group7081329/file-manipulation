A collection of powershell scripts to work with image files.

- folder-thumbnail-generator: 
Copy a file at random and create a file named "folder.jpg" and save it in the set folder. This is useful for image management applications that use folder.jpg for a thumbnail.
- set-thumbnail: 
Will prompt user for source and destination folders. It will scan the source folder recursively, choosing an image file at random from each set folder and copying to the destination. This is useful for thumnail indexes.
