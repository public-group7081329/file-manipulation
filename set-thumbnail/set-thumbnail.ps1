﻿# Prompt for the source and destination folders
$srcFolder = Read-Host "Enter the source folder path"
$dstFolder = Read-Host "Enter the destination folder path"

# Create the destination folder if it does not already exist
if (!(Test-Path $dstFolder))
{
    New-Item -ItemType Directory -Path $dstFolder
}

# Get a list of all subfolders in the source folder (including subfolders of subfolders, and so on)
$subFolders = Get-ChildItem $srcFolder -Directory -Recurse

# Get the total number of subfolders
$totalSubfolders = $subFolders.Count

# Prompt the user to confirm that they want to continue
$confirmMessage = "This operation will copy 1 file from each of $totalSubfolders subfolders. Do you want to continue?"
$continue = (Read-Host $confirmMessage) -eq "Y"

if ($continue)
{
    # Loop through each subfolder
    for ($i = 0; $i -lt $subFolders.Count; $i++)
    {
        # Get the current subfolder
        $subFolder = $subFolders[$i]

        # Get a list of all jpeg files in the current subfolder
        $jpegFiles = Get-ChildItem $subFolder.FullName -Filter *.jpg

        # Check if the current subfolder contains any jpeg files
        if ($jpegFiles.Count -gt 0)
        {
            # Select a random jpeg file from the current subfolder
            $randomJpeg = $jpegFiles | Get-Random

            # Check if the random jpeg file variable is null
            if ($randomJpeg -ne $null)
            {
                # Copy the random jpeg file to the destination folder with the source folder name appended to the file name
                Copy-Item $randomJpeg.FullName -Destination "$dstFolder\$($subFolder.Name)_$($randomJpeg.Name)" -ErrorAction SilentlyContinue
            }

            # Display the progress bar
            Write-Progress -Activity "Copying files" -Status "Processing $($subFolder.Name)" -PercentComplete (($i / $subFolders.Count) * 100)
        }
    }
}
